# Loading Dynamic Variables Into Process
## Assumptions
* Running JBoss BPM Suite 6.0.3 on EAP

## Usage
1. Add this project as a dependency in pom.xml/project editor.
2. Register DynamicVariableLoader as Process Event Listener in kmodule.xml.
```
<kmodule xmlns="http://jboss.org/kie/6.0.0/kmodule" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <kbase name="kbase" default="true" eventProcessingMode="stream" equalsBehavior="identity" scope="javax.enterprise.context.ApplicationScoped" packages="*">
    <ksession name="defaultKieSession" type="stateful" default="true" clockType="realtime" scope="javax.enterprise.context.ApplicationScoped">
      <listeners>
        <processEventListener type="example.DynamicVariableLoader"/>
      </listeners>
    </ksession>
  </kbase>
</kmodule>
```
3. Add `dynamic.properties` file to `JBOSS_HOME/standalone/deployments/business-central.war/WEB-INF/classes` with desired properties.
4. DynamicVariableLoader will automatically add properties as process variables to each new process started. These may be referenced in WorkItemHandlers, etc.