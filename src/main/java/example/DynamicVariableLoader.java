package example;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map.Entry;
import java.util.Properties;

import org.kie.api.event.process.DefaultProcessEventListener;
import org.kie.api.event.process.ProcessStartedEvent;
import org.kie.api.runtime.process.WorkflowProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DynamicVariableLoader extends DefaultProcessEventListener {

	private static final Logger LOG = LoggerFactory.getLogger("variableLoader");

	@Override
	public void beforeProcessStarted(ProcessStartedEvent event) {
		WorkflowProcessInstance process = (WorkflowProcessInstance) event
				.getProcessInstance();
		Properties variables = readVariables();
		updateVariables(variables, process);
	}

	// Read in variables from properties file
	private Properties readVariables() {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();

		Properties p = new Properties();
		try(InputStream is = cl.getResourceAsStream("dynamic.properties")) {
			if (is != null) {
				p.load(is);
			}
		} catch (IOException e) {
			LOG.warn("Unable to load dynamic properties", e);
		}
		
		return p;
	}

	// Populate process instance with variables
	private void updateVariables(Properties variables,
			WorkflowProcessInstance process) {
		for (Entry<Object, Object> v : variables.entrySet()) {
			process.setVariable((String) v.getKey(), (String) v.getValue());
		}
	}

}
